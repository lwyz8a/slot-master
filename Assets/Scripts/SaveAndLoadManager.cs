﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveAndLoadManager
{
    public const string FirstLaunchKey = "FirstLaunch"; //Key for Player.Prefs when checking for the first launch.

    private const string PlayerSaveFileName = "slotMaster.mcs";

    private static string savePath;
    public static string SavePath
    {
        get
        {
            if (string.IsNullOrEmpty(savePath))
            {
                savePath = Path.Combine(Application.persistentDataPath, PlayerSaveFileName);
            }
            return savePath;
        }
    }

    public static bool IsFirstTimeLaunched()
    {
        if (PlayerPrefs.GetInt(FirstLaunchKey, 0) == 0)
        {
            Debug.Log("First time launching the game.");
            PlayerPrefs.SetInt(FirstLaunchKey, 1); //Set the first launch now for future checks.
            return true;
        }

        Debug.Log("Not the first time launching the game.");
        return false;
    }

    public static bool SavePlayer(Player player)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(SavePath, FileMode.Create);
        try
        {
            formatter.Serialize(stream, player);
        }
        catch (System.Exception)
        {
            Debug.LogError("ERROR! Saving player data FAILED!");
            return false;
        }
        finally
        {
            stream.Close();
        }

        return true;
    }

    public static Player LoadPlayer()
    {
        Player player = null;
        if (!File.Exists(SavePath))
        {
            Debug.LogError($"ERROR! The save path {SavePath} doesn't exists!");
            return player;
        }

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(SavePath, FileMode.Open);
        try
        {
            player = formatter.Deserialize(stream) as Player;
        }
        catch (System.Exception)
        {
            Debug.LogError($"ERROR! Loading player data FAILED!");
            return null;
        }
        finally
        {
            stream.Close();
        }

        return player;
    }
}
