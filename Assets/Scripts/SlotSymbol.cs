﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotSymbol : MonoBehaviour
{
    public enum Type
    {
        Lucky7,
        Bell,
        Cherry,
        Watermelon,
        Heart,
        Lemon,
        Clover,
        NONE,
    }

    public Type type;
}
