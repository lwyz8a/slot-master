﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReelSpinner : MonoBehaviour
{
    public float spinSpeed = 7f;
    public Transform topPosition;
    public Transform bottomPosition;

    [HideInInspector]
    public bool spinning = false;

    private SlotSymbol[] slotSymbols;
    private float ogSpeed;

    // Start is called before the first frame update
    void Start()
    {
        slotSymbols = transform.GetComponentsInChildren<SlotSymbol>();
        ogSpeed = spinSpeed;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!spinning) { return; }
        foreach (var slot in slotSymbols)
        {
            slot.transform.Translate(Vector2.down * spinSpeed * Time.fixedDeltaTime);
            if (slot.transform.position.y <= bottomPosition.position.y)
            {
                slot.transform.position = topPosition.position;
            }
        }
    }

    public void StartSpinning()
    {
        spinning = true;
    }

    public void StopSpinning()
    {
        spinning = false;
    }

    public void ReduceSpindSpeed(float percentageReduction)
    {
        float reduction = spinSpeed * percentageReduction / 100f;
        spinSpeed -= reduction;
    }

    public void ResetSpinSpeed()
    {
        spinSpeed = ogSpeed;
    }
}
