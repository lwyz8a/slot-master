﻿using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.Reflection;

public class UIManager : MonoBehaviour
{
    public Text moneyText;
    public Text ticketsText;
    public Text prizeText;
    public Text levelText;
    public Button spinButton;
    public Button buyTicketButton;
    public Button levelUpButton;
    public GameObject prizeTablePanel;
    public GameObject levelAbilitiesPanel;

    private Text buyTicketText;
    private Text levelUpButtonText;
    private Text prizeTableText;
    private GameManager gameManager;
    private ProgressionManager progressionManager;

    public static UIManager Instance;
    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager.Instance;
        progressionManager = ProgressionManager.Instance;
        buyTicketText = buyTicketButton.GetComponentInChildren<Text>();
        levelUpButtonText = levelUpButton.GetComponentInChildren<Text>();
        prizeTableText = prizeTablePanel.GetComponentInChildren<Text>();

        moneyText.text = $"${gameManager.GetPlayerMoney():0.00}";
        ticketsText.text = $"{gameManager.GetPlayerTickets()} tickets";
        levelText.text = $"Level {gameManager.GetPlayerLevel()}";
        buyTicketText.text = $"Buy a Ticket for ${gameManager.ticketPrice}";
        SetLevelUpButtonText(gameManager.GetPlayerLevel());

        EnableSpinButton(gameManager.GetPlayerTickets());
        EnableBuyTicketButton(gameManager.GetPlayerMoney());
        EnableLevelUpButton(gameManager.GetPlayerMoney());
        SetPrizeTableText();

        GameManager.OnPlayerMoneyModified += SetMoneyText;
        GameManager.OnPlayerMoneyModified += EnableBuyTicketButton;
        GameManager.OnPlayerMoneyModified += EnableLevelUpButton;
        GameManager.OnPlayerTicketsModified += SetTicketsText;
        GameManager.OnPlayerTicketsModified += EnableSpinButton;
        GameManager.OnPrizeWon += SetPrizeText;
        GameManager.OnPlayerLevelUp += SetLevelText;
        GameManager.OnPlayerLevelUp += SetLevelUpButtonText;
        ProgressionManager.OnPrizeUpdate += SetPrizeTableText;
    }

    private void OnDestroy()
    {
        GameManager.OnPlayerMoneyModified -= SetMoneyText; 
        GameManager.OnPlayerMoneyModified -= EnableBuyTicketButton;
        GameManager.OnPlayerMoneyModified -= EnableLevelUpButton;
        GameManager.OnPlayerTicketsModified -= SetTicketsText;
        GameManager.OnPlayerTicketsModified -= EnableSpinButton;
        GameManager.OnPrizeWon -= SetPrizeText;
        GameManager.OnPlayerLevelUp -= SetLevelText; 
        GameManager.OnPlayerLevelUp -= SetLevelUpButtonText;
        ProgressionManager.OnPrizeUpdate -= SetPrizeTableText;
    }

    public void SetMoneyText(float newMoney)
    {
        moneyText.text = $"${newMoney:0.00}";
    }

    public void SetTicketsText(int newTickets)
    {
        ticketsText.text = $"{newTickets} tickets";
    }

    public void SetPrizeText(PrizeData prize)
    {
        prizeText.text = prize.prizeText + prize.moneyPrizeText;
    }

    public void SetLevelText(int newLevel)
    {
        levelText.text = $"Level {newLevel}";
    }

    public void SetLevelUpButtonText(int currentLevel)
    {
        int nextLevel = currentLevel + 1;
        levelUpButtonText.text = nextLevel > GameManager.MAX_LEVEL ? "You are now the MAX level!"
                : $"Level up to level {nextLevel} for ${progressionManager.levelPrices[nextLevel]}";
    }

    public void EnableSpinButton()
    {
        spinButton.enabled = gameManager.GetPlayerTickets() > 0;
    }

    public void ShowPrizeTable(bool show)
    {
        prizeTablePanel.SetActive(show);
    }

    public void ShowLevelAbilities(bool show)
    {
        levelAbilitiesPanel.SetActive(show);
    }

    private void EnableSpinButton(int currentTickets)
    {
        spinButton.enabled = currentTickets > 0;
    }

    private void EnableBuyTicketButton(float currentMoney)
    {
        buyTicketButton.enabled = currentMoney >= gameManager.ticketPrice;
    }

    private void EnableLevelUpButton(float currentMoney)
    {
        int nextLevel = gameManager.GetPlayerLevel() + 1;
        levelUpButton.enabled = nextLevel > GameManager.MAX_LEVEL ? false
                : currentMoney >= progressionManager.levelPrices[nextLevel];
    }

    private void SetPrizeTableText()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append($"Lucky 7 - ${gameManager.prizeDatas[5].moneyPrize:0.00}\n");
        sb.Append($"Double - ${gameManager.prizeDatas[4].moneyPrize:0.00}\n");
        sb.Append($"Double and one 7  - ${gameManager.prizeDatas[3].moneyPrize:0.00}\n");
        sb.Append($"Double 7 - ${gameManager.prizeDatas[2].moneyPrize:0.00}\n");
        sb.Append($"Triple - ${gameManager.prizeDatas[1].moneyPrize:0.00}\n");
        sb.Append($"Jackpot - ${gameManager.prizeDatas[0].moneyPrize:0.00}\n");
        prizeTableText.text = sb.ToString();
    }
}
