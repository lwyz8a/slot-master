﻿using UnityEngine;

/// <summary>
/// Handles abilities per level and achievements.
/// </summary>
public class ProgressionManager : MonoBehaviour
{
    public Timer reelTimer;
    [Tooltip("The percentage for the reels slow down."), Range(1f, 100f)]
    public float reelSpeedReduction = 50f;
    [Tooltip("The price to leveling up on each level, using the index as the level.")]
    public int[] levelPrices;

    public static event System.Action OnPrizeUpdate;

    private GameManager gameManager;
    private int reelsStopped;
    private bool[] canSlowDownReel = new bool[] { false, false, false };

    public static ProgressionManager Instance;
    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start() //In the script execution order, this Start will execute first, then the GameManager Start.
    {
        gameManager = GameManager.Instance;
        int playerLevel = gameManager.GetPlayerLevel();

        for (int level = 2; level <= playerLevel; level++)
        {
            AssignLevelAbilities(level);
        }

        GameManager.OnPlayerLevelUp += AssignLevelAbilities;
        GameManager.OnReelsStartSpinning += ReelsStartSpinning;
        GameManager.OnReelStopped += ReelStopped;
    }

    private void OnDestroy()
    {
        GameManager.OnPlayerLevelUp -= AssignLevelAbilities;
        GameManager.OnReelsStartSpinning -= ReelsStartSpinning;
        GameManager.OnReelStopped -= ReelStopped;
    }

    public void LevelUpPlayer()
    {
        gameManager.LevelUpPlayer(levelPrices[gameManager.GetPlayerLevel() + 1]);
    }

    public void CheckForSlowDown(int reelIndex)
    {
        Debug.Log("Checking for slow down...");
        if (canSlowDownReel[reelsStopped])
        {
            Debug.Log("Slowing down this reel!");
            gameManager.reelSpinners[reelIndex].ReduceSpindSpeed(reelSpeedReduction);
        }
    }

    public void ResetSlowDown(int reelIndex)
    {
        gameManager.reelSpinners[reelIndex].ResetSpinSpeed();
    }

    /// <summary>
    /// The abilities here will be hardcoded as to not over engineer with abilities classes and editor scripts.
    /// </summary>
    /// <param name="level"></param>
    private void AssignLevelAbilities(int level)
    {
        switch (level)
        {
            case 2:
                Debug.Log("Gain 5% more on prizes!");
                ModifyPrizePercentage(5f);
                break;
            case 3:
                Debug.Log("Gain 3 more seconds!");
                AddExtraSeconds(3);
                break;
            case 4:
                Debug.Log("Slow down the first reel you want to stop!");
                SlowDownReel(0);
                break;
            case 5:
                Debug.Log("Gain 4 more seconds and gain 4% more on prizes!");
                AddExtraSeconds(4);
                ModifyPrizePercentage(4f);
                break;
            case 6:
                Debug.Log("Slow down the last reel you want to stop!");
                SlowDownReel(2);
                break;
            case 7:
                Debug.Log("Gain 5% more on prizes and slow down the second reel you want to stop!");
                ModifyPrizePercentage(5f);
                SlowDownReel(1);
                break;
            default:
                break;
        }
    }

    private void ModifyPrizePercentage(float percentage)
    {
        for (int i = 0; i < gameManager.prizeDatas.Length; i++)
        {
            float currentPrize = gameManager.prizeDatas[i].moneyPrize;
            float percentagePrize = currentPrize * percentage / 100f;
            gameManager.prizeDatas[i].moneyPrize = currentPrize + percentagePrize;
        }

        OnPrizeUpdate?.Invoke();
    }

    private void AddExtraSeconds(int extraSeconds)
    {
        reelTimer.startingSeconds += extraSeconds;
    }

    private void SlowDownReel(int reelIndex)
    {
        canSlowDownReel[reelIndex] = true;
    }

    private void ReelsStartSpinning()
    {
        reelsStopped = 0;
    }

    private void ReelStopped(int reelIndex)
    {
        ++reelsStopped;
    }
}
