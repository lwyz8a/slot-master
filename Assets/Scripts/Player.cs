﻿[System.Serializable]
public class Player
{
    public float money;
    public int tickets;
    public int level;

    public Player(float money, int tickets)
    {
        this.money = money;
        this.tickets = tickets;
        level = 1;
    }

    public Player(float money, int tickets, int level)
    {
        this.money = money;
        this.tickets = tickets;
        this.level = level;
    }
}
